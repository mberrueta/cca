import urllib2, datetime, sys

url = sys.argv[1]
date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

try:
	req = urllib2.Request(url)
	res = urllib2.urlopen(req)
	print '%s url="%s" status="%s" message="%s"' % (date, res.url, res.code, res.read().rstrip())
except urllib2.URLError, e:
	print '%s url="%s" status="%s" message="%s - %s"' % (date, url, 0, 'URLError', e.reason)
except urllib2.HTTPError, e:
	print '%s url="%s" status="%s" message="%s - %s"' % (date, url, e.code, 'HTTPError', )