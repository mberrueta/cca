require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {

    // Translations from prioridadmap results to CSS class
    var ICONS = {
       "-2": 'linea',
        2: 'alert-circle',
        1: 'alert-circle',
        0: 'check-circle'
    };

   var prioridadMapIconRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the prioridad field
            return cell.field === 'estado';
        },
        render: function($td, cell) {
            var icon = 'question';
            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {
                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- prioridad %>" title="<%- prioridad %>"></i>', {
                icon: icon,
                prioridad: cell.value
            }));
        }
    });
	
	
	var prioridadMapIconRenderer_dos = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the prioridad field
			var allowedCells = ["AIS","RCM","IDB","AIS_DB","IFM_DB"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
			//return cell.field === 'AIS';
        },
        render: function($td, cell) {
            var icon = 'question';
            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {
                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
			var trick = cell.field;
			
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- DATO %>" title="<%- DATO %>"></i>', {
                icon: icon,
                DATO: cell.value
            }));
        }
    });
	
	
	mvc.Components.get('table1').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer_dos());
    });
	
	mvc.Components.get('table4').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer_dos());
    });
	
	mvc.Components.get('table5').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });
	

});
