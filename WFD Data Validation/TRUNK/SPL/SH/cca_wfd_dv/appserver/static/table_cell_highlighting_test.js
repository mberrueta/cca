require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {
     // Row Coloring Example with custom, client-side range interpretation
    var CustomRangeRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Enable this custom cell renderer for both the active_hist_searches and the active_realtime_searches field
            var allowedCells = ["AIS1_01","AIS1_02","AIS1_03","AIS1_04","AIS1_05","AIS1_06","AIS1_07","AIS1_08","AIS2_01","AIS2_02","AIS2_03","AIS2_04","AIS2_05","AIS2_06","AIS2_07","AIS2_08","AIS1B_01","AIS1B_02","AIS1B_03","AIS1B_04","AIS1B_05","AIS1B_06","AIS1B_07","AIS1B_08","AIS2B_01","AIS2B_02","AIS2B_03","AIS2B_04","AIS2B_05","AIS2B_06","AIS2B_07","AIS2B_08","cantidad","AIS3_03","AIS4_04","AIS5_MAINTENCE","AIS5_MANT","AIS3_01", "AIS4_01"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
        },
        render: function($td, cell) {
            // Add a class to the cell based on the returned value
			
			var elem = cell.value.split("-");
			var valor = parseFloat(elem[0]);
			var color = elem[1];
            //var amarillo = parseFloat(elem[1]);
		    //var rojo = parseFloat(elem[2]); 
			




            // Apply interpretation for number of historical searches
				

                if (color == "verde" || valor==0) {
                    $td.addClass('range-cell').addClass('range-low');
                }
                else if (color == "rojo" || valor!=0) {
                    $td.addClass('range-cell').addClass('range-severe');
                }
                else{
                    $td.addClass('range-cell').addClass('range-elevated');
                }



                /*if (valor < amarillo || valor == 0) {
                    $td.addClass('range-cell').addClass('range-low');
                }
				else if (valor >= rojo) {
                    $td.addClass('range-cell').addClass('range-severe');
                }
				else{
                    $td.addClass('range-cell').addClass('range-elevated');
                }*/

           
            // Update the cell content
            $td.text(valor).addClass('numeric');
        }
    });
    
	mvc.Components.get('highlight1').getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addCellRenderer(new CustomRangeRenderer());
    });
	
	    
	mvc.Components.get('highlight2').getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addCellRenderer(new CustomRangeRenderer());
    });
	
	
});




