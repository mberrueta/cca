require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {

    // Translations from prioridadmap results to CSS class
    var ICONS = {
        "1-rojo": 'alert-circle',
        " ": 'alert',
        "0-verde": 'check-circle'
    };
	
	var prioridadMapIconRenderer_dos = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the prioridad field
			var allowedCells = ["AIS1_01","AIS2_01","AIS3_01","AIS4_01","AIS5_MAINTENCE","Estado","flag"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
			//return cell.field === 'AIS';
        },
        render: function($td, cell) {
            var icon = '-';

            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {
                icon = ICONS[cell.value];
            }

            // Create the icon element and add it to the table cell
			var trick = cell.field;

			
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- DATO %>" title="<%- DATO %>"></i>', {
                icon: icon,
                DATO: cell.value
            }));
        }
    });
	
	
	
	mvc.Components.get('element1').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer_dos());
    });
	mvc.Components.get('element3').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer_dos());
    });
	mvc.Components.get('element5').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer_dos());
    });

});