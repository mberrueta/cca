require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {
     // Row Coloring Example with custom, client-side range interpretation
    var CustomRangeRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Enable this custom cell renderer for both the active_hist_searches and the active_realtime_searches field
            var allowedCells = ["AIS1_01","AIS2_01","AIS3_01","AIS4_01","AIS5_MAINTENCE","Estado"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
        },
        render: function($td, cell) {
            // Add a class to the cell based on the returned value
			
			var elem = cell.value.split("-");
			
            var valor = parseFloat(elem[0]);
			var color = elem[1];
            // Apply interpretation for number of historical searches
				
                if (color =="verde") {
                    $td.addClass('range-cell').addClass('range-low');
                }
				else{
                    $td.addClass('range-cell').addClass('range-severe');
                }

            // Update the cell content
            $td.text(valor).addClass('numeric');
        }
    });
    
	var CustomRangeRenderer_2 = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Enable this custom cell renderer for both the active_hist_searches and the active_realtime_searches field
            var allowedCells = ["estado","hora_inicio","hora_termino","duracion","cantidad_errores"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
        },
        render: function($td, cell) {
            // Add a class to the cell based on the returned value
			
			var elem = cell.value
			var color = elem;
			console.log("elem: "+elem)
            // Apply interpretation for number of historical searches
				
                if (color =="Failed" || color=="Running") {
                    $td.addClass('range-cell').addClass('range-severe');
					if(cell.field =="hora_inicio"||cell.field =="hora_termino"||cell.field =="duracion"||cell.field =="cantidad_errores"){
						$td.addClass('range-cell').addClass('range-severe');
					}
                }
				else if(color=="Finished successfully"){
                    $td.addClass('range-cell').addClass('range-low');
                }
			// Update the cell content
            $td.text(color).addClass('numeric');

        }
    });
	
	mvc.Components.get('element4').getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addCellRenderer(new CustomRangeRenderer());
    });
	mvc.Components.get('element2').getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addCellRenderer(new CustomRangeRenderer());
    });
	mvc.Components.get('element6').getVisualization(function(tableView) {
		  tableView.$el.find('td.range-cell').each(function() {
                $(this).parents('tr').addClass(this.className);
            });
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addCellRenderer(new CustomRangeRenderer_2());
    });
	
});




