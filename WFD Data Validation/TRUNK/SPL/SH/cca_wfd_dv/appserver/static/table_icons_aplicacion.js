require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {

    // Translations from prioridadmap results to CSS class
    var ICONS = {
        2: 'alert-circle',
        1: 'alert-circle',
        "-1": 'alert',
        0: 'check-circle'
    };
	

   var prioridadMapIconRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the prioridad field
            return cell.field === 'estado';
        },
        render: function($td, cell) {
            var icon = 'question';
            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {

                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- prioridad %>" title="<%- prioridad %>"></i>', {
                icon: icon,
                prioridad: cell.value
            }));
        }
    });
	

	
	var prioridadMapIconRenderer_dos = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the prioridad field
			var allowedCells = ["AIS1_01","AIS1_02","AIS1_03","AIS1_04","AIS1_05","AIS1_06","AIS1_07","AIS1_08","AIS2_01","AIS2_02","AIS2_03","AIS2_04","AIS2_05","AIS2_06","AIS2_07","AIS2_08","AIS1B_01","AIS1B_02","AIS1B_03","AIS1B_04","AIS1B_05","AIS1B_06","AIS1B_07","AIS1B_08","AIS2B_01","AIS2B_02","AIS2B_03","AIS2B_04","AIS2B_05","AIS2B_06","AIS2B_07","AIS2B_08","AIS3_03","AIS4_04","AIS5_MAINTENCE","AIS5_MANT", "AIS3_01", "AIS4_01", "MAINT_01"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
			//return cell.field === 'AIS';
        },
        render: function($td, cell) {
            var icon = 'question';

            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {
                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
			var trick = cell.field;

			
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- DATO %>" title="<%- DATO %>"></i>', {
                icon: icon,
                DATO: cell.value
            }));
        }
    });
	
	
	
	var prioridadMapIconRenderer_tres = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the prioridad field
            return cell.field === ' ';
        },
        render: function($td, cell) {
            var icon = 'question';
            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {

                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- DATO %>" title="<%- DATO %>"></i>', {
                icon: icon,
                DATO: cell.value
            }));
        }
    });

	
	
	
	mvc.Components.get('table1').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer_dos());
    });


	mvc.Components.get('table3').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });
	
	
	mvc.Components.get('table5').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });
	
	/*
	mvc.Components.get('table3').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });
	
	mvc.Components.get('table5').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });
	*/

});
