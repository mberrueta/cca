require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {

    // Aca se añaden los valores y los iconos los cuales representaran 0(Icono verde), 1 o 2 (Icono Rojo)
    var ICONS = {
        1: 'alert-circle',
        2: 'alert-circle',
        "-1": 'alert',
        0: 'check-circle'
    };
	

   var prioridadMapIconRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Nombre de columna que se va pintar
            return cell.field === 'estado';
        },
        render: function($td, cell) {
            var icon = 'question';
            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {
                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- prioridad %>" title="<%- prioridad %>"></i>', {
                icon: icon,
                prioridad: cell.value
            }));
        }
    });
	
/*
	
	var prioridadMapIconRenderer_dos = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the prioridad field
			var allowedCells = ["AIS1_01","AIS1_02","AIS1_03","AIS1_04","AIS1_05","AIS1_06","AIS1_07","AIS1_08","AIS2_01","AIS2_02","AIS2_03","AIS2_04","AIS2_05","AIS2_06","AIS2_07","AIS2_08"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
			//return cell.field === 'AIS';
        },
        render: function($td, cell) {
            var icon = 'question';
            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {
                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
			var trick = cell.field;
			
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- DATO %>" title="<%- DATO %>"></i>', {
                icon: icon,
                DATO: cell.value
            }));
        }
    });
	
	
	
	var prioridadMapIconRenderer_tres = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the prioridad field
            return cell.field === ' ';
        },
        render: function($td, cell) {
            var icon = 'question';
            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {
                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- DATO %>" title="<%- DATO %>"></i>', {
                icon: icon,
                DATO: cell.value
            }));
        }
    });

	*/
	
	
	mvc.Components.get('table1').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });

	mvc.Components.get('table8').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });
	mvc.Components.get('table2').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });

    mvc.Components.get('table3').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });

    mvc.Components.get('table4').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });    
	/*
	mvc.Components.get('table3').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer_tres());
    });
	
	
	mvc.Components.get('table5').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });*/
	
	/*
	mvc.Components.get('table3').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });
	
	mvc.Components.get('table5').getVisualization(function(tableView){
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new prioridadMapIconRenderer());
    });
	*/

});