require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {
     // Row Coloring Example with custom, client-side range interpretation
    var CustomRangeRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Enable this custom cell renderer for both the active_hist_searches and the active_realtime_searches field
			var allowedCells = ["Tps e","Tps p","TRX","Error","TimeOut","Rechazo","Reinsistencia","Desafio","Alertas"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
        },
        render: function($td, cell) {
            // Add a class to the cell based on the returned value
			
			var elem = cell.value.split("-");
			
            var valor = parseFloat(elem[0]);
			var umbral = parseFloat(elem[1]);
			//var rojo = parseFloat(elem[2]); 
			
            // Apply interpretation for number of historical searches
 
                if (umbral == 0) {
                    $td.addClass('range-cell').addClass('range-low');
                }
				else if (umbral==1) {
                    
                    $td.addClass('range-cell').addClass('range-elevated');
                }
				else{
                    
                    $td.addClass('range-cell').addClass('range-severe');
                }

            // Update the cell content
			$td.text(valor).addClass('numeric');
			valor = valor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
			$td.text(valor);
        }
    });
    
	mvc.Components.get('highlight1').getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addCellRenderer(new CustomRangeRenderer());
    });

	
});

