require([
	'splunkjs/mvc/tableview',
	'splunkjs/mvc/chartview',
	'splunkjs/mvc/searchmanager',
	'splunkjs/mvc',
	'underscore',
	'splunkjs/mvc/simplexml/ready!'],function(
	TableView,
	ChartView,
	SearchManager,
	mvc,
	_
	){
			
	var ICONS = {
		"1-rojo": 'alert-circle',
		" ": 'alert',
		"0-verde": 'check-circle'
	};
	
	var EventSearchBasedRowExpansionRenderer = TableView.BaseRowExpansionRenderer.extend({
		initialize: function(args) {
			this._searchManager = new SearchManager({
				id: 'details-search-manager',
				preview: false,
			});
			this._tableView = new TableView({
				managerid: 'details-search-manager',
				drilldown: "none",
				id: 'subTable',
			});
		},
		canRender: function(rowData) {
			return true;
		},
		render: function($container, rowData) {
			var ComponenteCell = _(rowData.cells).find(function (cell) {
			return cell.field === 'componente';
			});

			this._searchManager.set({ search: 'index=cca_wfd sourcetype=st_heartbeat earliest="-5m" latest="now" | lookup heartbeat url OUTPUT | search padre_name="'+ComponenteCell.value+'" AND name!="'+ComponenteCell.value+'" | dedup url  | rename name AS componente | eval server_instancia=server."_".instancia | chart max(priority) as priority over componente by server_instancia | foreach AIS* [eval <<FIELD>>=case(\'<<FIELD>>\'=0, "0-verde", isnull(\'<<FIELD>>\'), "", 1==1, "1-rojo")]| table componente AIS1_01, AIS2_01, AIS3_01, AIS4_01, AIS5_MAINTENCE'});
			$container.append(this._tableView.render().el);
			
		} 
	}); 
	
	 var EventSearchBasedRowExpansionRenderer_2 = TableView.BaseRowExpansionRenderer.extend({
		initialize: function(args) {
			this._searchManager = new SearchManager({
				id: 'details-search-manager-2',
				preview: false
			});
			this._tableView2 = new TableView({
				managerid: 'details-search-manager-2',
				drilldown: "none",
				id: 'subBancos',
				"format": {"Transacciones": [{"type": "sparkline", "options": {"barWidth": "5px","colorMap": {"1:": "#04B404","0:0": "#FF0000"},"type": "bar", "height": "8px"}}]}
			});
		},
		canRender: function(rowData) {
			return true;
			},
		render: function($container, rowData) {
			var bancoCell = _(rowData.cells).find(function (cell) {
			return cell.field === 'Banco';
			});

			this._searchManager.set({ search: '| inputlookup bancos.csv   | search bank_group="OTROS" | table bank_name, orden, bank_group  | join type=left bank_name [ search   index="cca_wfd_summary" query=summary_transacciones_sniffer bank_name!="UNKNOWN" earliest="-32m" latest="-2m@m"  | bucket span=1m _time | chart count by bank_name, _time limit=0 ]  | fillnull value=0  | script countZero | join type=left bank_name [ search  index="cca_wfd_summary" query=summary_transacciones_sniffer bank_name!="UNKNOWN" earliest="-32m" latest="-2m@m"  | bucket span=1m _time   | stats sparkline(count, 1m) as trend by bank_name ]  | makemv delim="," setsv=true trend  | table bank_group, bank_name, flag, trend, orden  | sort +orden, -flag  | eval flag=if(flag=0, "0-verde", "1-rojo") | fields - orden bank_group|rename bank_name as Banco,flag as Estado,trend as Transacciones| table Banco Estado Transacciones'});
			$container.append(this._tableView2.render().el);
			
		} 
		
			// Create an instance of the custom cell renderer
			//var myCellRenderer = new prioridadMapIconRenderer_dos();

			// Add the custom cell renderer to the table
			//_tableView2.addCellRenderer(myCellRenderer); 

			// Render the table
			//_tableView2.render();

	});

	
	var myVar=setInterval(myFunction, 500);
	var tableElement = mvc.Components.getInstance("element1");
	tableElement.getVisualization(function(tableView) {
		tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer());
	});
	
	var myVar2=setInterval(myFunction2, 500);
	var tableElement2 = mvc.Components.getInstance("element5");
	tableElement2.getVisualization(function(tableView) {
		tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer_2());
	});
	
	function myFunction() {
		x=$("#element1 .row-expansion-toggle");
		y=$("#element1 .row-expansion-toggle a");
		
		 if (x.length>2){
			for (var i=0;i<x.length;i++){
				if (i!=2 && i!=3){
				 y[i].remove();
				 x[i].classList.remove("row-expansion-toggle");
				}
			}
		 }
	}
	
	function myFunction2() {
		x=$("#element5 .row-expansion-toggle");
		y=$("#element5 .row-expansion-toggle a");
		
		 if (x.length>1){
			for (var i=0;i<x.length;i++){
				if (i!=4){
				 y[i].remove();
				 x[i].classList.remove("row-expansion-toggle");
				}
			}
		 }
	}
	
	
	var prioridadMapIconRenderer = TableView.BaseCellRenderer.extend({
		canRender: function(cell) {
			// Only use the cell renderer for the prioridad field
			//var allowedCells = ["Estado","flag"];
			var allowedCells = ["AIS1_01","AIS2_01","AIS3_01","AIS4_01","AIS5_MAINTENCE","Estado","flag"];
			return (allowedCells.indexOf(cell.field) > -1 ) ;
			//return cell.field === 'AIS';
		},
		render: function($td, cell) {
			var icon = '-';

			// Fetch the icon for the value
			if (ICONS.hasOwnProperty(cell.value)) {
				icon = ICONS[cell.value];
			}

			// Create the icon element and add it to the table cell
			var trick = cell.field;

			$td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- DATO %>" title="<%- DATO %>"></i>', {
				icon: icon,
				DATO: cell.value
			}));
		}
	});



	mvc.Components.get('subTable').getVisualization(function(tableView){
		tableView.addCellRenderer(new prioridadMapIconRenderer());
	})
	mvc.Components.get('subBancos').getVisualization(function(tableView){
		tableView.addCellRenderer(new prioridadMapIconRenderer());
	});

	
});